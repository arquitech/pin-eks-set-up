#!/bin/bash
# Set up AWS CLI client
aws configure set default.region us-east-1 && aws configure set default.output yaml 2>&1 | tee -a aws_set_up_log.txt

# Clone EKS repo to set up cluster
git clone https://bitbucket.org/arquitech/pin-eks-set-up.git && sleep 20 2>&1 | tee -a git_set_up_log.txt

# Set up EKS with Terraform
cd /home/ubuntu/pin-eks-set-up && terraform init 2>&1 | tee -a terraform_eks_set_up_log.txt
cd /home/ubuntu/pin-eks-set-up && terraform apply -auto-approve 2>&1 | tee -a terraform_eks_set_up_log.txt

# Complete Terraform setup
cd /home/ubuntu/pin-eks-set-up && aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name) 2>&1 | tee -a terraform_eks_set_up_log.txt

# Prepare cluster account to integrate to azure devops
cd /home/ubuntu && git clone https://bitbucket.org/arquitech/azdo.git && sleep 20 2>&1 | tee -a git_set_up_log.txt
cd /home/ubuntu/azdo && kubectl apply -f ado-admin-service-account.yaml 2>&1 | tee -a azdo_set_up_log.txt
SECRET_NAME=$(kubectl get serviceAccounts ado -n kube-system -o=jsonpath={.secrets[*].name})
echo "export SECRET_NAME=$SECRET_NAME" >> ~/.profile
cd /home/ubuntu/azdo && kubectl get secret $SECRET_NAME -n kube-system -o json 2>&1 | tee -a azdo_set_up_log.txt
cd /home/ubuntu/azdo && kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}' 2>&1 | tee -a azdo_set_up_log.txt