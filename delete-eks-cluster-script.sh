#!/bin/bash
# Set up EKS with Terraform
cd /home/ubuntu/pin-eks-set-up && terraform init | tee -a terraform_eks_delete_log.txt
cd /home/ubuntu/pin-eks-set-up && terraform destroy -auto-approve | tee -a terraform_eks_delete_log.txt